<h1 align="center"> ✅ desafio-books-frontend </h1>

Estes documento README tem como objetivo fornecer as informações necessárias para realização do **projeto ioasys books**.

# ✅ Tabela de Conteúdos

- Status do Projeto
- Tecnologias]
- Utilitários
- Como rodar a aplicação
- Contribuição

<br/>
<h1 > ✅ Status do Projeto </h1>

<h4 align="start"> 
	🚧  Em aprimoramento...  🚧
</h4>

<h1 > ✅ Tecnologias </h1>

#### **Tecnologias**

- React
  - Context - API
  - CRA
- Styled Components
- Material UI
- Axios
- React-router-dom
- React-hook-form
- Yup
- @resolvers/yup

# **Servidor utilizado** ([Api][ioasys books api])

## **Documentação da API**

- Documentação: https://books.ioasys.com.br/api/docs/#/Books/getBook

## **Servidor da API**

- Servidor: https://books.ioasys.com.br/api/v1
  <br />

### **EndPoints Utilizados**

- /auth/sign-in
- /books
- /books/{id}

<h2> ✅ Utilitários </h2>

- [Commit Conventional](https://www.conventionalcommits.org/en/v1.0.0/)
- [Teste de API (Insomnia)](https://insomnia.rest/download)
- [Protótipo (Figma)](https://www.figma.com/file/YXuqJUzNZcR7GveJfVWCKo/Desafio-Frontend%3A-ioasys-books)
- [Editor (Visual Studio Code)](https://code.visualstudio.com/download)
- [Plataforma para a hospedagem (vercel)](https://vercel.com)

<h2> ✅ Como rodar a aplicação </h2>

## Pré-requisitos

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
Git, Node.js, yarn.

<br />

## Instalando yarn via npm

É recomendável instalar o Yarn por meio do gerenciador de pacotes npm , que vem junto com o Node.js quando você o instala em seu sistema.

<br />

Depois de instalar o npm, você pode executar o seguinte para instalar e atualizar o Yarn:

     npm install --global yarn

### Verifique a instalação

Verifique se o Yarn está instalado executando:

    yarn --version

## Clonando o repositório do Gitlab

    $ git clone https://gitlab.com/JoaoJr/project-ioasys

## Abra no Visual Studio Code

    $ code .

## No diretório do projeto, você pode executar:

### `yarn`

para atualizar o **package.json**

### `yarn start`

Executa o aplicativo no modo de desenvolvimento.

Abra [http://localhost:3000](http://localhost:3000) para visualizá-lo no navegador.

A página será recarregada se você fizer edições. \
Você também verá quaisquer erros de lint no console.

<h2>  ✅ Contribuição </h2>

<div>
 <table style="width:50%">
 <tr>
    <th><strong>João Alves</strong></th>
  </tr>
  <tr >
    <td>
      <a href="http://www.linkedin.com/in/joaoalvesdasilvajunior"
         target="_blank"
         rel="noreferrer">
      <img
      style="border-radius: 50%;"
        height="120"
        width="120"
        alt="goat"
        src="src/Assets/Img/Dev/dev.jpeg">
        </a>
    </td>
   </tr>
 </table>
</div>
