import { createContext, useState, useContext, useEffect } from "react";
import api from "../../Services/api";
import { useAuth } from "../Auth";

export const IdBooksContext = createContext();

export const IdBooksProvider = ({ children }) => {
  const { auth } = useAuth();
  const [idBooks, setIdBooks] = useState();
  const [dataBook, setDataBook] = useState();

  useEffect(() => {
    if (idBooks) {
      api
        .get(`/books/${idBooks}`, {
          headers: {
            Authorization: `Bearer ${auth}`,
          },
        })
        .then((response) => setDataBook(response.data))
        .catch((e) => console.log(e));
    }
  }, [idBooks]);

  return (
    <IdBooksContext.Provider value={{ idBooks, setIdBooks, dataBook }}>
      {children}
    </IdBooksContext.Provider>
  );
};

export const useIdBooks = () => useContext(IdBooksContext);
