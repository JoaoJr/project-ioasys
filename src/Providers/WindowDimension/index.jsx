import { createContext, useState, useContext, useEffect } from "react";

export const WindowContext = createContext();

export const WindowProvider = ({ children }) => {
  const hasWindow = typeof window !== "undefined";
  const getWindowDimensions = () => {
    const width = window.innerWidth;
    return {
      width,
    };
  };

  const [dimensions, setDimensions] = useState(getWindowDimensions());
  const handleResize = () => {
    setDimensions(getWindowDimensions());
  };

  useEffect(() => {
    if (hasWindow) {
      handleResize();

      window.addEventListener("resize", handleResize);
      return () => window.removeEventListener("resize", handleResize);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [hasWindow]);

  return (
    <WindowContext.Provider value={{ dimensions, setDimensions }}>
      {children}
    </WindowContext.Provider>
  );
};

export const useWindow = () => useContext(WindowContext);
