import { createContext, useState, useContext } from "react";

export const PaginateContext = createContext();

export const PaginateProvider = ({ children }) => {
  const [page, setPage] = useState(1);
  const [amount, setAmount] = useState(12);
  const [disabled, setDisabled] = useState(false);

  const addCounter = () => {
    setPage(page + 1);
    setDisabled(false);
  };

  const subCounter = () => {
    setPage(page - 1);
    if (page <= 1) {
      setPage(1);
      setDisabled(true);
    }
  };

  return (
    <PaginateContext.Provider
      value={{
        page,
        setPage,
        amount,
        setAmount,
        addCounter,
        subCounter,
        disabled,
        setDisabled,
      }}
    >
      {children}
    </PaginateContext.Provider>
  );
};

export const usePaginate = () => useContext(PaginateContext);
