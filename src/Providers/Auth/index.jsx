import { createContext, useContext, useState } from "react";
import api from "../../Services/api";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const token = localStorage.getItem("token") || "";

  const [auth, setAuth] = useState(token);
  const [user, setUser] = useState({});

  const [connected, setConnected] = useState(false);

  const signIn = (userData, setError, navigate) => {
    setError(false);
    api
      .post("/auth/sign-in", userData)
      .then((response) => {
        localStorage.clear();
        localStorage.setItem("token", response.headers.authorization);
        setAuth(localStorage.getItem("token"));
        setUser(response.data);
        setConnected(true);
        navigate("/home");
      })
      .catch((err) => {
        setError(true);
      });
  };

  return (
    <AuthContext.Provider
      value={{
        auth,
        setAuth,
        user,
        setUser,
        connected,
        setConnected,
        signIn,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => useContext(AuthContext);
