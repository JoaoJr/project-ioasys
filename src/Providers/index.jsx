import { AuthProvider } from "./Auth";
import { ErrorProvider } from "./Error";
import { PaginateProvider } from "./Paginate";
import { IdBooksProvider } from "./IdBooks";
import { WindowProvider } from "./WindowDimension";

const Providers = ({ children }) => {
  return (
    <WindowProvider>
      <ErrorProvider>
        <AuthProvider>
          <IdBooksProvider>
            <PaginateProvider>{children}</PaginateProvider>
          </IdBooksProvider>
        </AuthProvider>
      </ErrorProvider>
    </WindowProvider>
  );
};

export default Providers;
