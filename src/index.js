import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

import GlobalStyle from "./Styles/global";
import { BrowserRouter } from "react-router-dom";
import Providers from "./Providers";

ReactDOM.render(
  <React.StrictMode>
    <Providers>
      <BrowserRouter>
        <GlobalStyle />
        <App />
      </BrowserRouter>
    </Providers>
  </React.StrictMode>,
  document.getElementById("root")
);
