import { createGlobalStyle } from "styled-components";

export default createGlobalStyle`

*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
}

ul {
      list-style-type: none;
}

li {
list-style-type: none;
    
}

:root {
    --black: #333333;
    --gray: #999999;
    --pink: #b22e6f;
    --white: #fff;


}

html {
    background: #E5E5E5;;
}

body{
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    margin: 0 auto;
    width: 100vw;
    height: 100vh;
    max-width: 1366px;

   
}

.App{
  font-family: 'Heebo', sans-serif;
}
`;
