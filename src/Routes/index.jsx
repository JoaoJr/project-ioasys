import { Routes, Route } from "react-router-dom";
import LoginPage from "../Pages/LoginPage";
import HomePage from "../Pages/HomePage";

import { useAuth } from "../Providers/Auth";

const RoutesPages = () => {
  const { connected } = useAuth();

  return (
    <>
      <Routes>
        {connected ? (
          <Route path="home" element={<HomePage />} />
        ) : (
          <Route path="*" element={<LoginPage />} />
        )}
        <Route path="/" element={<LoginPage />} />
      </Routes>
    </>
  );
};

export default RoutesPages;
