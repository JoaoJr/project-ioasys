import { ContainerStyled } from "./styles";
import ImgVet from "../../Assets/Img/Login/vector.svg";

import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useNavigate } from "react-router-dom";

import { useAuth } from "../../Providers/Auth";
import { useError } from "../../Providers/Error";

const Login = () => {
  const navigate = useNavigate();
  const { signIn } = useAuth();
  const { error, setError } = useError();

  const schema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().required(),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    signIn(data, setError, navigate);
    reset();
  };

  return (
    <>
      <ContainerStyled>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="input-email input-desktop">
            <label>Email</label>
            <input name="email" {...register("email")} />
          </div>
          <div className="input-password input-desktop">
            <div>
              <label>Senha</label>
              <input
                type={"password"}
                name="password"
                {...register("password")}
              />
            </div>
            <button type="submit">Entrar</button>
          </div>
        </form>
        {error && (
          <div className="margin-box-erro">
            <img src={ImgVet} alt="vector" />
            <div className="box-error">
              <span> Email e/ou senha incorretos. </span>
            </div>
          </div>
        )}
      </ContainerStyled>
    </>
  );
};

export default Login;
