import styled from "styled-components";
export const ContainerStyled = styled.div`
  .input-email {
    width: 288px;
    height: 60px;
    padding: 6px 16px;
    border-radius: 4px;
    background: rgba(0, 0, 0, 0.32);

    display: flex;
    flex-direction: column;

    label {
      width: 30px;
      height: 16px;
      font-weight: 400;
      font-size: 12px;
      line-height: 16px;

      color: var(--white);

      opacity: 0.5;
    }

    input {
      width: 250px;
      height: 24px;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;

      color: var(--white);
      background: transparent;
      border: none;
    }
  }

  .input-password {
    width: 288px;
    height: 60px;
    background: rgba(0, 0, 0, 0.32);
    border-radius: 4px;
    margin-top: 16px;
    padding: 6px 16px;

    display: flex;
    align-items: center;
    justify-content: space-between;

    div {
      display: flex;
      flex-direction: column;
    }
    label {
      width: 30px;
      height: 16px;
      font-weight: 400;
      font-size: 12px;
      line-height: 16px;

      color: var(--white);

      opacity: 0.5;
    }

    input {
      width: 168px;
      height: 24px;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;

      color: var(--white);
      background: transparent;
      border: none;
    }

    button {
      width: 85px;
      height: 36px;
      border-radius: 44px;

      cursor: pointer;

      font-weight: bold;
      font-size: 16px;
      line-height: 20px;
      color: var(--pink);
      border: none;
    }
  }

  .margin-box-erro {
    width: 239px;
    height: 56px;
    background: rgba(255, 255, 255, 0.4);
    border-radius: 4px;
    margin-top: 16px;
    display: flex;
    align-items: center;
    justify-content: center;
    color: var(--white);
    font-weight: bold;
    font-size: 16px;
    line-height: 16px;
    position: absolute;

    img {
      position: absolute;
      left: 18px;
      top: -10px;
      width: 16px;
      height: 12px;
    }
  }

  @media screen and (min-width: 321px) {
    .input-desktop {
      width: 368px;
    }
    .input-email {
      input {
        width: 250px;
      }
    }
  }
`;
