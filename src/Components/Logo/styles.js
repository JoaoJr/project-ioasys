import styled from "styled-components";
export const ContainerStyled = styled.div`
  display: flex;
  align-items: center;

  width: 198px;
  height: 40px;

  img {
    width: 104px;
    height: 36px;
  }

  h3 {
    height: 40px;
    width: 77px;

    font-weight: 300;
    font-size: 28px;
    line-height: 40px;
    margin-left: 16px;
  }
  .white {
    color: #fff;
  }

  .black {
    color: #000;
  }
`;
