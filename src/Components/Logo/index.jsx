import { ContainerStyled } from "./styles";
import ImgLogo1 from "../../Assets/Img/logo/Logo1.svg";
import ImgLogo2 from "../../Assets/Img/logo/Logo2.svg";

const Logo = ({ mode }) => {
  return (
    <ContainerStyled>
      <>
        {mode === 1 ? (
          <>
            <img src={ImgLogo1} alt="logo da empresa" />
            <h3 className="white">Books</h3>
          </>
        ) : (
          <>
            <img src={ImgLogo2} alt="logo da empresa" />
            <h3 className="black">Books</h3>
          </>
        )}
      </>
    </ContainerStyled>
  );
};

export default Logo;
