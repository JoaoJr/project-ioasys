import styled from "styled-components";

export const ContainerStyled = styled.div`
  background: var(--white);
  width: 272px;
  height: 160px;
  background: var(--white);
  box-shadow: 0px 6px 24px rgba(84, 16, 95, 0.13);
  border-radius: 4px;
  border: none;

  display: flex;

  :hover {
    cursor: pointer;
  }

  .container-img {
    width: 114px;
    height: 160px;
    display: flex;
    align-items: center;
    justify-content: center;

    img {
      width: 81px;
      height: 103px;
    }
  }

  .container-info-book {
    width: 132px;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;
    text-align: left;

    h4 {
      width: 132px;
      height: 20px;

      font-family: "Heebo";
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      line-height: 20px;
      color: var(--black);
    }

    .p-authors {
      display: flex;
      flex-wrap: wrap;
      font-family: "Heebo";
      font-style: normal;
      font-weight: 400;
      font-size: 12px;
      line-height: 20px;
      color: var(--pink);
    }
    .p-description {
      p {
        font-family: "Heebo";
        font-style: normal;
        font-weight: 400;
        font-size: 12px;
        line-height: 20px;
        color: var(--gray);
      }
    }
  }
`;
