import { ContainerStyled } from "./styles";
import ImgBook from "../../Assets/Img/Home/Book.svg";

const CardBook = ({ data }) => {
  return (
    <ContainerStyled>
      <figure className="container-img">
        {data.imageUrl ? (
          <img src={data.imageUrl} alt="capa do livro" />
        ) : (
          <img src={ImgBook} alt="capa do livro" />
        )}
      </figure>
      <div className="container-info-book">
        <h4>
          {data.title.length > 15
            ? `${data.title.substring(0, 15)}...`
            : data.title}
        </h4>
        <div className="p-authors">
          {data.authors.map((author, index) => {
            return (
              <p className="p-authors" key={index}>
                {author}
              </p>
            );
          })}
        </div>

        <div className="p-description">
          <p>{data.pageCount} páginas</p>

          <p>
            Editora
            {data.publisher.length > 15
              ? `${data.publisher.substring(0, 15)}...`
              : data.publisher}
          </p>
          <p>Publicado em {data.published}</p>
        </div>
      </div>
    </ContainerStyled>
  );
};

export default CardBook;
