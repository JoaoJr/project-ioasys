import { ContainerStyled } from "./styles";
import ImgBook from "../../Assets/Img/Home/Book.svg";
import ImgQuote from "../../Assets/Img/Home/quote.svg";

const CardBookModal = ({ data }) => {
  const {
    imageUrl,
    pageCount,
    publisher,
    published,
    language,
    title,
    isbn10,
    isbn13,
    authors,
    description,
  } = data;
  return (
    <ContainerStyled>
      {data && (
        <>
          <figure className="container-img">
            {data.imageUrl ? (
              <img src={imageUrl} alt="capa do livro" />
            ) : (
              <img src={ImgBook} alt="capa do livro" />
            )}
          </figure>
          <div className="container-info">
            <div className="book-mainInformation">
              <h3>
                {title.length > 30 ? `${title.substring(0, 30)}...` : title}
              </h3>
              <ul>
                {authors.map((author, index) => {
                  return <li key={index}>{author}, </li>;
                })}
              </ul>
            </div>

            <div className="book-details">
              <h4>informações</h4>
              <p className="p-info">
                <span>Páginas</span>
                <p>{pageCount} páginas</p>
              </p>
              <p className="p-info">
                <span>Editora</span>
                <p>{publisher}</p>
              </p>
              <p className="p-info">
                <span>Publicação</span>
                <p>{published}</p>
              </p>
              <p className="p-info">
                <span>Idioma</span>
                <p>{language}</p>
              </p>
              <p className="p-info">
                <span>Título Original</span>
                <p>{title}</p>
              </p>

              <p className="p-info">
                <span>ISBN-10</span>
                <p>{isbn10}</p>
              </p>
              <p className="p-info">
                <span>ISBN-13</span>
                <p>{isbn13}</p>
              </p>
            </div>

            <div className="book-review">
              <h4>Resenha da editora</h4>

              <p>
                <img src={ImgQuote} alt="quote" />
                {description}
              </p>
            </div>
          </div>
        </>
      )}
    </ContainerStyled>
  );
};

export default CardBookModal;
