import styled from "styled-components";

export const ContainerStyled = styled.div`
  .container-img {
    width: 240px;
    height: 351px;
    margin-bottom: 24px;
    filter: drop-shadow(0px 11.9008px 17.8512px rgba(0, 0, 0, 0.3));

    img {
      width: 100%;
      height: 100%;
    }
  }

  .container-info {
    display: flex;
    flex-direction: column;
  }

  .book-mainInformation {
    width: 175px;
    h3 {
      font-weight: 500;
      font-size: 28px;
      line-height: 40px;
      width: 240px;
      height: 80px;
      color: var(--black);
    }

    ul {
      font-weight: 400;
      font-size: 12px;
      line-height: 20px;
      width: 240px;
      height: 20px;
      color: var(--pink);

      display: flex;
      flex-wrap: wrap;

      li {
        margin-right: 2px;
      }
    }
  }

  .book-details {
    width: 84px;
    height: 240px;
    font-weight: 500;
    font-size: 12px;
    line-height: 28px;
    color: var(--black);

    h4 {
      text-transform: uppercase;
    }

    .p-info {
      width: 240px;
      display: flex;
      justify-content: space-between;

      p {
        font-weight: 400;
        text-align: right;
        color: var(--gray);
      }
    }
  }

  .book-review {
    width: 240px;
    font-weight: 500;
    font-size: 12px;
    line-height: 20px;
    color: var(--black);

    h4 {
      text-transform: uppercase;
      margin-bottom: 16px;
    }

    p {
      color: var(--gray);
      img {
        margin-right: 10px;
      }
    }
  }

  @media screen and (min-width: 321px) {
    display: flex;
    .container-img {
      width: 349px;
      height: 512.29px;
      margin-bottom: 0px;
    }

    .container-info {
      width: 276px;
      height: 512px;
      margin-left: 48px;
      overflow: hidden;
    }

   
    .book-details {
      margin-top: 32px;
      width: 84px;
      height: 220px;
   
      .p-info {
        justify-content: space-between;
      }
    }

    .book-review {
      width: 275px;
      height: 220px;
     
      margin-top: 32px;
      overflow: auto;

      ::-webkit-scrollbar {
        width: 4px;
      }

      ::-webkit-scrollbar-track {
        border-radius: 10px;
        background-color: none;
      }

      ::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #ab2680;
      }

      }
    }
  }
`;
