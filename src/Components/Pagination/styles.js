import styled from "styled-components";
export const ContainerStyled = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;

  p {
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 20px;

    color: var(--black);
  }
  .btnPrev {
    transform: scaleX(-1);
  }
`;
