import { ContainerStyled } from "./styles";
import Iconbtn from "../../Assets/Img/Home/btnNext.svg";
import IconButton from "@mui/material/IconButton";
import { usePaginate } from "../../Providers/Paginate";

const Pagination = ({ pageData, totalPage, mobile }) => {
  const { subCounter, addCounter, disabled } = usePaginate();

  return (
    <ContainerStyled>
      {mobile ? (
        <>
          <p>
            Página {pageData} de {totalPage.toFixed(0)}
          </p>
          <IconButton
            aria-label="btnPrev"
            size="small"
            onClick={subCounter}
            disabled={disabled}
          >
            <img src={Iconbtn} alt="btnPrev" className="btnPrev" />
          </IconButton>
          <IconButton aria-label="btnNext" size="small" onClick={addCounter}>
            <img src={Iconbtn} alt="btnNext" />
          </IconButton>
        </>
      ) : (
        <>
          <IconButton
            aria-label="btnPrev"
            size="small"
            onClick={subCounter}
            disabled={disabled}
          >
            <img src={Iconbtn} alt="btnPrev" className="btnPrev" />
          </IconButton>
          <p>
            Página {pageData} de {totalPage.toFixed(0)}
          </p>
          <IconButton aria-label="btnNext" size="small" onClick={addCounter}>
            <img src={Iconbtn} alt="btnNext" />
          </IconButton>
        </>
      )}
    </ContainerStyled>
  );
};

export default Pagination;
