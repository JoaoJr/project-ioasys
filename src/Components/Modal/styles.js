import styled from "styled-components";
export const ContainerStyled = styled.div`
  width: 100%;
  height: 100vh;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10;

  background: rgba(0, 0, 0, 0.5);

  border: none;

  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 64px 16px 16px 16px;

  .close {
    position: absolute;
    top: 16px;
    left: 272px;

    width: 32px;
    height: 32px;
    padding: 8px;

    background: var(--white);
    border: 1px solid rgba(51, 51, 51, 0.2);
    border-radius: 50%;

    display: flex;
    align-items: center;
    justify-content: center;

    :before,
    :after {
      content: " ";
      position: absolute;
      width: 1px;
      height: 8px;
      background: var(--black);
    }
    :before {
      transform: rotate(45deg);
    }

    :after {
      transform: rotate(-45deg);
    }
  }

  .modal-container {
    width: 288px;
    background: var(--white);
    box-shadow: 0px 16px 80px rgba(0, 0, 0, 0.32);
    border-radius: 4px;
    padding: 24px 0;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    overflow: auto;
  }

  @media screen and (min-width: 321px) {
    align-items: center;
    padding: 0;

    .close {
      left: 1317px;
    }

    .modal-container {
      width: 769px;
      height: 608px;
    }
  }
`;
