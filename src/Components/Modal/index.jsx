import { ContainerStyled } from "./styles";

const Modal = ({ children, handleClose }) => {
  return (
    <ContainerStyled>
      <button onClick={handleClose} className="close" />
      <div className="modal-container">{children}</div>
    </ContainerStyled>
  );
};

export default Modal;
