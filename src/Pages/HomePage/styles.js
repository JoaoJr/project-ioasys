import styled from "styled-components";
import BackgroundImg from "../../Assets/Img/Home/bgHome.png";

export const ContainerStyled = styled.div`
  width: 320px;
  height: 640px;
  overflow: auto;
  background: url(${BackgroundImg}) no-repeat;
  background-size: cover;
  background-blend-mode: darken;

  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 40px;

  header {
    width: 288px;
    height: 40px;

    font-weight: 400;
    font-size: 12px;
    line-height: 16px;
    text-align: right;
    color: var(--black);

    display: flex;
    justify-content: space-around;
    align-items: center;

    button {
      border: none;
      background: none;

      width: 32px;
      height: 32px;
      margin-left: 16px;
    }
  }

  .container-cards {
    width: 288px;
    display: flex;
    justify-content: center;

    li {
      margin: 8px 0;
    }

    .btn-container {
      border: none;

      .container-img {
        img {
          width: 81px;
          height: 122px;
        }
      }

      .container-info-book {
        width: 149px;
      }
    }

    .pagination {
      display: flex;
      justify-content: center;

      button {
        margin: 0 14px;
      }
    }
  }
  @media screen and (min-width: 321px) {
    width: 1365px;
    height: 768px;

    header {
      width: 1130px;
      height: 40px;
      margin: 40px 120px 40px 115px;

      display: flex;
      justify-content: space-between;
      align-items: center;

      .welcome {
        width: 169px;
        height: 32px;
        display: flex;
      }
    }

    .container-cards {
      width: 1136px;
      height: 512px;
      display: flex;
      justify-content: center;

      ul {
        width: 100%;
        height: 100%;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
      }

      li {
        margin: 0;
      }
    }

    .pagination {
      width: 1130px;
      height: 40px;

      justify-content: flex-end;

      button {
        margin: 0;
      }
    }
  }
`;
