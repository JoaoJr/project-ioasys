import { useState, useEffect } from "react";
import { ContainerStyled } from "./styles";

import api from "../../Services/api";

import logOut from "../../Assets/Img/Home/Logout.svg";
import Logo from "../../Components/Logo";
import CardBook from "../../Components/CardBook";
import Pagination from "../../Components/Pagination";
import Modal from "../../Components/Modal";
import CardBookModal from "../../Components/CardBookModal";

import { useAuth } from "../../Providers/Auth";
import { usePaginate } from "../../Providers/Paginate";
import { useIdBooks } from "../../Providers/IdBooks";
import { useWindow } from "../../Providers/WindowDimension";
import { useNavigate } from "react-router-dom";

const HomePage = () => {
  const navigate = useNavigate();

  const { user, setConnected, setUser, auth } = useAuth();
  const { setIdBooks, dataBook } = useIdBooks();
  const { page, amount } = usePaginate();
  const { dimensions } = useWindow();

  const [data, setData] = useState({});
  const [open, setOpen] = useState(false);

  const handleIdBooks = (id) => setIdBooks(id);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const handleLogout = () => {
    setConnected(false);
    setUser({});
    localStorage.clear();
    navigate("/");
  };

  useEffect(() => {
    if (page) {
      api
        .get(`/books?page=${page}&amount=${amount}`, {
          headers: {
            Authorization: `Bearer ${auth}`,
          },
        })
        .then((response) => setData(response.data))
        .catch((e) => console.log(e));
    }
  }, [page, amount, auth]);

  let width = parseInt(dimensions.width);

  console.log(dataBook);

  return (
    <ContainerStyled>
      <header>
        <Logo styleLogo={false} />
        <div className="welcome">
          {width >= 321 && (
            <p>
              Bem vindo, <span> {user.name}</span>!
            </p>
          )}
          <button onClick={handleLogout}>
            <img src={logOut} alt=" icone logout" />
          </button>
        </div>
      </header>
      {data.data && (
        <>
          <section className="container-cards">
            <ul>
              {data.data.map((data, index) => {
                return (
                  <li key={index}>
                    <button className="btn-container" onClick={handleOpen}>
                      <div onClick={() => handleIdBooks(data.id)}>
                        <CardBook data={data} />
                      </div>
                    </button>
                  </li>
                );
              })}
            </ul>
          </section>
          <div className="pagination">
            {width >= 321 ? (
              <Pagination
                pageData={data.page}
                totalPage={data.totalPages}
                mobile={true}
              />
            ) : (
              <Pagination
                pageData={data.page}
                totalPage={data.totalPages}
                mobile={false}
              />
            )}
          </div>
        </>
      )}

      {dataBook !== undefined && (
        <>
          {open && (
            <Modal handleClose={handleClose}>
              <CardBookModal data={dataBook} />
            </Modal>
          )}
        </>
      )}
    </ContainerStyled>
  );
};

export default HomePage;
