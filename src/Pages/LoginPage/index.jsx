import { ContainerStyled } from "./styles";
import Login from "../../Components/FormLogin";
import Logo from "../../Components/Logo";

const LoginPage = () => {
  return (
    <ContainerStyled>
      <header>
        <Logo mode={1} />
      </header>
      <section className="form-login">
        <Login />
      </section>
    </ContainerStyled>
  );
};

export default LoginPage;
