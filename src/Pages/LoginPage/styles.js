import styled from "styled-components";
import BackgroundImg from "../../Assets/Img/Login/bg.jpg";
import BackgroundImgMobile from "../../Assets/Img/LoginMobile/bgMobile.jpg";

export const ContainerStyled = styled.div`
  width: 100%;
  height: 768px;
  padding: 0 16px;
  display: flex;
  flex-direction: column;
  background: url(${BackgroundImgMobile}) no-repeat;

  header {
    margin: 210px 0 50px 0px;
  }
  @media screen and (min-width: 321px) {
      background: url(${BackgroundImg}) no-repeat;
      margin: 0;
      padding: 0 0 0 115px;

      justify-content: flex-start;
      align-items: flex-start;

      header {
        margin: 274px 0 50px 0;
      }
    }
  }
`;
